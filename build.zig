const std = @import("std");
const Build = std.Build;

pub fn build(b: *Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const libudev = b.addStaticLibrary(.{
        .name = "udev",
        .target = target,
        .optimize = optimize,
    });
    libudev.installHeader("src/libudev/libudev.h", "libudev.h");
    libudev.addCSourceFiles(.{
        .files = &.{"src/libudev/libudev.c"},
    });
    libudev.addIncludePath(.{ .path = "src/shared" });
    libudev.addIncludePath(.{ .path = "src/systemd" });
    libudev.addIncludePath(.{ .path = "src/basic" });
    libudev.linkLibC();

    libudev.defineCMacro("_GNU_SOURCE", "1");

    // TODO: Actually test target for these values
    libudev.defineCMacro("HAVE_SECURE_GETENV", "1");
    libudev.defineCMacro("HAVE_DECL_GETTID", "1");
    libudev.defineCMacro("HAVE_DECL_NAME_TO_HANDLE_AT", "1");
    libudev.defineCMacro("HAVE_DECL_SETNS", "1");
    libudev.defineCMacro("HAVE_DECL_RENAMEAT2", "1");
    libudev.defineCMacro("SIZEOF_PID_T", b.fmt("{}", .{@sizeOf(std.os.pid_t)}));
    libudev.defineCMacro("SIZEOF_UID_T", b.fmt("{}", .{@sizeOf(std.os.uid_t)}));
    libudev.defineCMacro("SIZEOF_GID_T", b.fmt("{}", .{@sizeOf(std.os.gid_t)}));
    libudev.defineCMacro("SIZEOF_TIME_T", b.fmt("{}", .{@sizeOf(std.os.time_t)}));
    libudev.defineCMacro("SIZEOF_RLIM_T", b.fmt("{}", .{@sizeOf(std.os.rlim_t)}));

    b.installArtifact(libudev);
}
